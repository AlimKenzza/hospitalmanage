package com.aitu.hospitalmanage.controllers;


import com.aitu.hospitalmanage.model.Appointment;
import com.aitu.hospitalmanage.services.AppointmentService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class AppointmentController {
    private final AppointmentService appointmentService;

    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }
    @GetMapping(path = "/appointment")
    public List<Appointment> getAllAppointments() {
        return appointmentService.getAllAppointments();
    }
    @GetMapping(path = "/appointment/{id}")
    public Appointment getAppointmentId(@PathVariable int id) {
        return appointmentService.getAppointmentById(id);
    }
    @PostMapping(path = "/appointment")
    public String createAppointment(@RequestBody Appointment appointment) {
        appointmentService.createAppointment(appointment);
        return "Appointment created successfully";
    }
    @DeleteMapping(path = "/appointment/{id}")
    public void deleteAppointmentById(@PathVariable int id) {
        appointmentService.deleteAppointmentById(id);
    }
}
