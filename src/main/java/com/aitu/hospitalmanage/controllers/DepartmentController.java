package com.aitu.hospitalmanage.controllers;

import com.aitu.hospitalmanage.model.Department;
import com.aitu.hospitalmanage.services.DepartmentService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DepartmentController {
    private final DepartmentService departmentService;

    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }
    @GetMapping(path = "/department")
    public List<Department> getAllDepartments() {
        return departmentService.getAllDepartments();
    }
    @GetMapping(path = "/department/{id}")
    public Department getDepartmentById(@PathVariable int id) {
        return departmentService.getDepartmentById(id);
    }
    @PostMapping(path ="/department")
    public String createDepartment(@RequestBody Department department) {
        departmentService.createDepartment(department);
        return "Department added successfully";
    }
    @DeleteMapping(path="department/{id}")
    public void deleteDepartmentById(@PathVariable int id) {
        departmentService.deleteDepartmentById(id);
    }

}
