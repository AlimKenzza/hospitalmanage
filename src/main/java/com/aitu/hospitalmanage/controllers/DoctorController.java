package com.aitu.hospitalmanage.controllers;


import com.aitu.hospitalmanage.model.Doctor;
import com.aitu.hospitalmanage.services.DoctorService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class DoctorController {
    private final DoctorService doctorService;

    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping(path = "/doctors")
    public List<Doctor> getAllDoctors() {
        return doctorService.getAllDoctors();
    }

    //getDoctorById
    @GetMapping(path = "/doctors/{id}")
    public Doctor getDoctorById(@PathVariable int id) {
        return doctorService.getDoctorById(id);
    }


    @PostMapping(path = "/doctors")
    public String createDoctor(@RequestBody Doctor doctor) {
        doctorService.createDoctor(doctor);
        return "Doctor added successfully";
    }

    @PutMapping(path = "/doctors/update")
    public String updateDoctor(@RequestBody Doctor doctor){
        doctorService.updateDoctor(doctor);
        return "Updated";
    }


    @DeleteMapping(path = "/doctors/{id}")
    public void deleteDoctorById(@PathVariable int id) {
        doctorService.deleteDoctorById(id);
    }
}
