package com.aitu.hospitalmanage.controllers;

import com.aitu.hospitalmanage.model.Doctor;
import com.aitu.hospitalmanage.model.Illness;
import com.aitu.hospitalmanage.services.IllnessService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class IllnessController {
    private final IllnessService illnessService;

    public IllnessController(IllnessService illnessService) {
        this.illnessService = illnessService;
    }
    @GetMapping(path = "/illnesses")
    public List<Illness> getAllIllnesses() {
        return illnessService.getAllInsurances();
    }
    @PostMapping(path="/illness")
    public String createIllness(@RequestBody Illness illness) {
        illnessService.createIllness(illness);
        return "Illness added successfully";
    }

}
