package com.aitu.hospitalmanage.controllers;


import com.aitu.hospitalmanage.model.Insurance;
import com.aitu.hospitalmanage.repositories.CrudRepository;
import com.aitu.hospitalmanage.repositories.InsuranceRepository;
import com.aitu.hospitalmanage.services.InsuranceService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class InsuranceController {
    private final InsuranceService insuranceService;

    public InsuranceController(InsuranceService insuranceService) {
        this.insuranceService = insuranceService;
    }
    @GetMapping(path = "/insurance")
    public List<Insurance> getAllInsurances() {
        return insuranceService.getAllInsurances();
    }


    @GetMapping(path = "/insurance/{id}")
    public Insurance getInsuranceById(@PathVariable int id) {
      return insuranceService.getInsuranceById(id);
    }
    @GetMapping(path = "/insurance/patient/{id}")
    public Insurance getInsuranceByPatientId(@PathVariable int id) {
        return insuranceService.getInsuranceByPatientId(id);
    }
    @PostMapping(path="/insurance")
    public String createInsurance(@RequestBody Insurance insurance) {
        insuranceService.createInsurance(insurance);
        return "Insurance added successfully";
    }


}
