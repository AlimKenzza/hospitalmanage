package com.aitu.hospitalmanage.controllers;



import com.aitu.hospitalmanage.model.Patient;
import com.aitu.hospitalmanage.services.PatientService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/patients")
public class PatientController {
    private final PatientService PATIENTSERVICE;

    public PatientController(PatientService patientService) {
        this.PATIENTSERVICE = patientService;
    }
    @GetMapping(path = "")
    public List<Patient> getAllDoctors() {
        return PATIENTSERVICE.getAllPatients();
    }

    //getDoctorById
   @GetMapping(path = "/{id}")
    public Patient getPatientById(@PathVariable int id) {
      return PATIENTSERVICE.getPatientById(id);
    }


    @PostMapping(path = "")
    public String createPatient(@RequestBody Patient patient) {
        PATIENTSERVICE.createPatient(patient);
        return "Patient added successfully";
    }


   @DeleteMapping(path = "/patients/{id}")
   public void deletePatientById(@PathVariable int id) {
      PATIENTSERVICE.deletePatientById(id);
   }
}
