package com.aitu.hospitalmanage.controllers;

import com.aitu.hospitalmanage.model.Schedule;
import com.aitu.hospitalmanage.services.ScheduleService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ScheduleController {
    private final ScheduleService scheduleService;

    public ScheduleController(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }
    @GetMapping(path="/schedule/{schedule_id}/{doctor_id}/{appointment_id}")
    public Schedule implementId(@PathVariable int schedule_id, @PathVariable int doctor_id, @PathVariable int appointment_id) {
        return scheduleService.getConnect(schedule_id,doctor_id,appointment_id);
    }
}
