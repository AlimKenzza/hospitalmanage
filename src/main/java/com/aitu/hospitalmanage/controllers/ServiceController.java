package com.aitu.hospitalmanage.controllers;


import com.aitu.hospitalmanage.model.Service_id;
import com.aitu.hospitalmanage.services.ServiceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ServiceController {
    private final ServiceService serviceService;

    public ServiceController(ServiceService serviceService) {
        this.serviceService = serviceService;
    }
    @GetMapping(path = "/services")
    public List<Service_id> getAllServiceId() {
        return serviceService.getAllServiceId();
    }
}
