package com.aitu.hospitalmanage.exceptions;

public class PatientNotFoundException  extends RuntimeException{
    public PatientNotFoundException(String message) {
        super(message);
    }
}
