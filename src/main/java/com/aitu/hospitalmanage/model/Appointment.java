package com.aitu.hospitalmanage.model;

import java.util.Date;

public class Appointment {
    public int getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(int appointmentId) {
        this.appointmentId = appointmentId;
    }

    public Date getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(Date appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public long getOfficeNo() {
        return officeNo;
    }

    public void setOfficeNo(long officeNo) {
        this.officeNo = officeNo;
    }

    private int appointmentId;
    private Date appointmentDate;
    private String appointmentTime;
    private long officeNo;
}
