package com.aitu.hospitalmanage.model;

public class Doctor {
    private int doctorId;
    private String doctorFname;
    private String doctorLname;
    private long iin;
    private long phoneNo;

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorFname() {
        return doctorFname;
    }

    public void setDoctorFname(String doctorFname) {
        this.doctorFname = doctorFname;
    }

    public String getDoctorLname() {
        return doctorLname;
    }

    public void setDoctorLname(String doctorLname) {
        this.doctorLname = doctorLname;
    }

    public long getIin() {
        return iin;
    }

    public void setIin(long iin) {
        this.iin = iin;
    }

    public long getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(long phoneNo) {
        this.phoneNo = phoneNo;
    }
}
