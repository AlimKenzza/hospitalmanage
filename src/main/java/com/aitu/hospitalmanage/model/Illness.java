package com.aitu.hospitalmanage.model;

public class Illness {
    private String illnessName;
    private String illnessDuration;
    private int depId;
    private String illnessHistory;
    private int productId;

    public String getIllnessName() {
        return illnessName;
    }

    public void setIllnessName(String illnessName) {
        this.illnessName = illnessName;
    }

    public String getIllnessDuration() {
        return illnessDuration;
    }

    public void setIllnessDuration(String illnessDuration) {
        this.illnessDuration = illnessDuration;
    }

    public String getIllnessHistory() {
        return illnessHistory;
    }

    public void setIllnessHistory(String illnessHistory) {
        this.illnessHistory = illnessHistory;
    }

    public int getDepId() {
        return depId;
    }

    public void setDepId(int depId) {
        this.depId = depId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

}
