package com.aitu.hospitalmanage.model;

public class Patient {
    private int patientId;
    private String patientFname;
    private String patientLname;
    private long patientIin;
    private long phoneNo;
    private Insurance insuranceId;
    private Insurance companyName;

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getPatientFname() {
        return patientFname;
    }

    public void setPatientFname(String patientFname) {
        this.patientFname = patientFname;
    }

    public String getPatientLname() {
        return patientLname;
    }

    public void setPatientLname(String patientLname) {
        this.patientLname = patientLname;
    }

    public long getPatientIin() {
        return patientIin;
    }

    public void setPatientIin(long patientIin) {
        this.patientIin = patientIin;
    }

    public long getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(long phoneNo) {
        this.phoneNo = phoneNo;
    }


    public Insurance getInsuranceId() {
        return insuranceId;
    }

    public void setInsuranceId(Insurance insuranceId) {
        this.insuranceId = insuranceId;
    }

    public Insurance getCompanyName() {
        return companyName;
    }

    public void setCompanyName(Insurance companyName) {
        this.companyName = companyName;
    }
}
