package com.aitu.hospitalmanage.model;

import java.util.Date;

public class Schedule {
    private int scheduleId;
    private Date appointmentDate;
    private String appointmentTime;
    private long officeNo;
    private String doctorFname;
    private String doctorLname;
    private long phoneNo;

    public int getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(int scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Date getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(Date appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public long getOfficeNo() {
        return officeNo;
    }

    public void setOfficeNo(long officeNo) {
        this.officeNo = officeNo;
    }

    public String getDoctorFname() {
        return doctorFname;
    }

    public void setDoctorFname(String doctorFname) {
        this.doctorFname = doctorFname;
    }

    public String getDoctorLname() {
        return doctorLname;
    }

    public void setDoctorLname(String doctorLname) {
        this.doctorLname = doctorLname;
    }

    public long getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(long phoneNo) {
        this.phoneNo = phoneNo;
    }
}
