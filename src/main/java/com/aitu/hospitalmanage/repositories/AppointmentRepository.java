package com.aitu.hospitalmanage.repositories;


import com.aitu.hospitalmanage.model.Appointment;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class AppointmentRepository implements CrudRepository {
    private JdbcTemplate jdbcTemplate;

    public AppointmentRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }




    public Appointment findAppointmentById(int id) {
        String sql = "Select * FROM appointment WHERE appointment_id=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<>(Appointment.class));
    }


    public void deleteById(int id) {

    }

    public List<Appointment> getAllAppointments() {
        String sql = "SELECT * FROM appointment";
        return jdbcTemplate.query(sql, new RowMapper<Appointment>() {
            @Override
            public Appointment mapRow(ResultSet resultSet, int i) throws SQLException {
                Appointment appointment = new Appointment();
                appointment.setAppointmentId(resultSet.getInt(1));
                appointment.setAppointmentDate(resultSet.getDate(2));
                appointment.setAppointmentTime(resultSet.getString(3));
                appointment.setOfficeNo(resultSet.getLong(4));
                return appointment;
            }
        });
    }

    public void insertAppointment(Appointment appointment) {
        String sql = "INSERT INTO appointment values (?,?,?,?)";
        jdbcTemplate.update(sql, appointment.getAppointmentId(), appointment.getAppointmentDate(), appointment.getAppointmentTime(), appointment.getOfficeNo());
    }

    public void deleteAppointmentById(int id) {
        String sql = "DELETE FROM appointment WHERE appointment_id=?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public List findAll() {
        return null;
    }

    @Override
    public void findById(int id) {

    }
}
