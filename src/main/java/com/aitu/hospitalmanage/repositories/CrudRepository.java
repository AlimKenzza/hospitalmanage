package com.aitu.hospitalmanage.repositories;

import com.aitu.hospitalmanage.model.Appointment;

import java.util.List;

public interface CrudRepository {
    List findAll();
    void findById(int id);
    void deleteById(int id);
}
