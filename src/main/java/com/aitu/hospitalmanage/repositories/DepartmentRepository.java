package com.aitu.hospitalmanage.repositories;


import com.aitu.hospitalmanage.model.Department;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class DepartmentRepository {
    private JdbcTemplate jdbcTemplate;

    public DepartmentRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Department> getAllDepartments() {
        String sql = "SELECT * FROM department";
        return jdbcTemplate.query(sql, new RowMapper<Department>() {
            @Override
            public Department mapRow(ResultSet resultSet, int i) throws SQLException {
                Department department = new Department();
                department.setDepId(resultSet.getInt(1));
                department.setDepName(resultSet.getString(2));
                department.setDepNo(resultSet.getLong(3));
                return department;
            }
        });
    }


    public Department getDepartmentById(int id) {
        String sql = "Select * from department where dep_id=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<>(Department.class));
    }

    public void insertDepartment(Department department) {
        String sql = "Insert into department values (?,?,?)";
        jdbcTemplate.update(sql, department.getDepId(), department.getDepName(), department.getDepNo());
    }

    public void deleteDepartment(int id) {
        String sql = "DELETE FROM department WHERE dep_id=?";
        jdbcTemplate.update(sql, id);
    }
}
