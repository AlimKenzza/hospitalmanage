package com.aitu.hospitalmanage.repositories;


import com.aitu.hospitalmanage.exceptions.DoctorNotFoundException;
import com.aitu.hospitalmanage.model.Doctor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class DoctorRepository implements CrudRepository {
    private JdbcTemplate jdbcTemplate;

    public DoctorRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }




    public Doctor getDoctorById(int id) {
        String sql = "Select * FROM doctors WHERE doctor_id=?";
        Doctor doctor = null;
        try {
            doctor = jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<>(Doctor.class));
        }
        catch (Exception e) {
            throw new DoctorNotFoundException("Doctor not found : "+id);
        }
        return doctor;
    }

    public void insertDoctor(Doctor doctor) {
        String sql = "INSERT INTO doctors VALUES (?,?,?,?,?)";
        jdbcTemplate.update(sql, doctor.getDoctorId(), doctor.getDoctorFname(), doctor.getDoctorLname(), doctor.getIin(), doctor.getPhoneNo());
    }


    public void deleteDoctor(int id) {
        String sql = "DELETE FROM doctors WHERE doctor_id=?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public List<Doctor> findAll() {
        String sql = "Select * from doctors";
        return jdbcTemplate.query(sql, new RowMapper<Doctor>() {
            @Override
            public Doctor mapRow(ResultSet resultSet, int i) throws SQLException {
                Doctor doctor = new Doctor();
                doctor.setDoctorId(resultSet.getInt(1));
                doctor.setDoctorFname(resultSet.getString(2));
                doctor.setDoctorLname(resultSet.getString(3));
                doctor.setIin(resultSet.getLong(4));
                doctor.setPhoneNo(resultSet.getLong(5));
                return doctor;
            }
        });
    }

    @Override
    public void findById(int id) {
    }

    @Override
    public void deleteById(int id) {

    }

    public void updateDoctor(Doctor doctor) {
        String sql = "UPDATE doctors SET doctor_fname = 'Kydirbay' where doctor_id = 7;";
        jdbcTemplate.update(sql,Doctor.class);
    }
}
