package com.aitu.hospitalmanage.repositories;


import com.aitu.hospitalmanage.model.Illness;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class IllnessRepository implements CrudRepository {
    private JdbcTemplate jdbcTemplate;

    public IllnessRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Illness> getAllInsurances() {
        String sql = "SELECT * FROM illness";
        return jdbcTemplate.query(sql, new RowMapper<Illness>() {
            @Override
            public Illness mapRow(ResultSet resultSet, int i) throws SQLException {
                Illness illness = new Illness();
                illness.setIllnessName(resultSet.getString(1));
                illness.setIllnessDuration(resultSet.getString(2));
                illness.setDepId(resultSet.getInt(3));
                illness.setIllnessHistory(resultSet.getString(4));
                illness.setProductId(resultSet.getInt(5));
                return illness;
            }
        });
    }

    public void insertIllness(Illness illness) {
        String sql = "INSERT INTO illness VALUES (?,?,?,?,?)";
        jdbcTemplate.update(sql, illness.getIllnessName(), illness.getIllnessDuration(), illness.getDepId(), illness.getIllnessHistory(),illness.getProductId());
    }

    @Override
    public List findAll() {
        return null;
    }

    @Override
    public void findById(int id) {
    }

    @Override
    public void deleteById(int id) {
    }
}
