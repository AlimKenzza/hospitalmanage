package com.aitu.hospitalmanage.repositories;


import com.aitu.hospitalmanage.model.Insurance;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class InsuranceRepository implements CrudRepository{
    private JdbcTemplate jdbcTemplate;


    public InsuranceRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;

    }

    public List<Insurance> getAllInsurances() {
        String sql = "Select * from insurance";
        return jdbcTemplate.query(sql, new RowMapper<Insurance>() {
            @Override
            public Insurance mapRow(ResultSet resultSet, int i) throws SQLException {
                Insurance insurance = new Insurance();
                insurance.setInsuranceId(resultSet.getInt(1));
                insurance.setCompanyName(resultSet.getString(2));
                insurance.setInsuranceType(resultSet.getString(3));
                insurance.setValidity(resultSet.getString(4));
                insurance.setPatientId(resultSet.getInt(5));
                return insurance;
            }
        });
    }


    public Insurance getInsuranceById(int id) {
        String sql = "SELECT * FROM insurance WHERE insurance_id=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<>(Insurance.class));
    }

    public Insurance getInsuranceByPatientId(int id) {
        String sql = "SELECT * FROM insurance WHERE patient_id=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<>(Insurance.class));
    }

    public void insertInsurance(Insurance insurance) {
        String sql = "INSERT INTO insurance VALUES (?,?,?,?,?)";
        jdbcTemplate.update(sql, insurance.getInsuranceId(), insurance.getCompanyName(),insurance.getInsuranceType(), insurance.getValidity(), insurance.getPatientId());
    }

    @Override
    public List findAll() {
        return null;
    }

    @Override
    public void findById(int id) {
    }

    @Override
    public void deleteById(int id) {

    }
}
