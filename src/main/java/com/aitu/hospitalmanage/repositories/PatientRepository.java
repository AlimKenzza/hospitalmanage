package com.aitu.hospitalmanage.repositories;




import com.aitu.hospitalmanage.exceptions.DoctorNotFoundException;
import com.aitu.hospitalmanage.model.Patient;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PatientRepository implements CrudRepository {
    private final JdbcTemplate jdbcTemplate;
    private InsuranceRepository insuranceRepository;

    public PatientRepository(JdbcTemplate jdbcTemplate, InsuranceRepository insuranceRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.insuranceRepository = insuranceRepository;
    }

    /* public List<Patient> getAllPatients() {
        String sql = "Select * from patients";
        return jdbcTemplate.query(sql, (resultSet, i) -> {
            Patient patient = new Patient();
            patient.setPatient_id(resultSet.getInt(1));
            patient.setPatient_fname(resultSet.getString(2));
            patient.setPatient_lname(resultSet.getString(3));
            patient.setPatient_iin(resultSet.getLong(4));
            patient.setPhone_no(resultSet.getLong(5));
          //  patient.setCompany_name(insuranceRepository.getInsuranceById(patient.getInsurance_id()));
            return patient;
        });
    }

     */

    public Patient getPatientById(int id) {
        String sql = "SELECT * FROM patients WHERE patient_id=?";
        Patient patient = null;
        try {
            patient = jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<>(Patient.class));
        }
        catch (Exception e) {
            throw new DoctorNotFoundException("Patient not found : "+id);
        }
        return patient;
    }

    public void insertPatient(Patient patient) {
        String sql = "INSERT INTO patients VALUES(?,?,?,?,?)";
        jdbcTemplate.update(sql, patient.getPatientId(),patient.getPatientFname(), patient.getPatientLname(),patient.getPatientIin(),patient.getPhoneNo());
    }

    public void deletePatient(int id) {
        String sql = "DELETE FROM patients where patient_id=?";
        jdbcTemplate.update(sql,id);
    }

    @Override
    public List<Patient> findAll() {
        String sql = "Select * from patients";
        return jdbcTemplate.query(sql, (resultSet, i) -> {
            Patient patient = new Patient();
            patient.setPatientId(resultSet.getInt(1));
            patient.setPatientFname(resultSet.getString(2));
            patient.setPatientLname(resultSet.getString(3));
            patient.setPatientIin(resultSet.getLong(4));
            patient.setPhoneNo(resultSet.getLong(5));
            //  patient.setCompany_name(insuranceRepository.getInsuranceById(patient.getInsurance_id()));
            return patient;
        });
    }

    @Override
    public void findById(int id) {

    }

    @Override
    public void deleteById(int id) {

    }
}
