package com.aitu.hospitalmanage.repositories;


import com.aitu.hospitalmanage.model.Schedule;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ScheduleRepository {
    private JdbcTemplate jdbcTemplate;

    public ScheduleRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Schedule getConnect(int schedule_id, int doctor_id, int appointment_id) {
        String sql = "Select appointment_date, appointment_time, office_NO, doctor_fname, doctor_lname,\n" +
                "phone_NO, schedule_id from schedule inner join doctors on\n" +
                "(schedule.doctor_id=doctors.doctor_id) inner join appointment on\n" +
                "(schedule.appointment_id=appointment.appointment_id);";
        return jdbcTemplate.queryForObject(sql,new Object[]{schedule_id,doctor_id,appointment_id}, new BeanPropertyRowMapper<>(Schedule.class));
    }



}
