package com.aitu.hospitalmanage.repositories;

import com.aitu.hospitalmanage.model.Service_id;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ServiceRepository implements CrudRepository {
    private JdbcTemplate jdbcTemplate;

    public ServiceRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    public List<Service_id> getAllServiceId() {
        String sql = "Select * from service_id";
        return jdbcTemplate.query(sql, new RowMapper<Service_id>() {
            @Override
            public Service_id mapRow(ResultSet resultSet, int i) throws SQLException {
                Service_id service_id = new Service_id();
                service_id.setServiceId(resultSet.getInt(1));
                service_id.setDoctorId(resultSet.getInt(2));
                service_id.setPatientId(resultSet.getInt(3));
                service_id.setDepId(resultSet.getInt(4));
                return service_id;
            }
        });
    }

    @Override
    public List findAll() {
        return null;
    }

    @Override
    public void findById(int id) {
    }

    @Override
    public void deleteById(int id) {

    }
}
