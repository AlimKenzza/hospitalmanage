package com.aitu.hospitalmanage.services;


import com.aitu.hospitalmanage.model.Appointment;
import com.aitu.hospitalmanage.repositories.AppointmentRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AppointmentService {
    private final AppointmentRepository appointmentRepository;

    public AppointmentService(AppointmentRepository appointmentRepository) {
        this.appointmentRepository = appointmentRepository;
    }

    public List<Appointment> getAllAppointments() {
        return appointmentRepository.getAllAppointments();
    }

    public Appointment getAppointmentById(int id) {
        return appointmentRepository.findAppointmentById(id);
    }

    public void createAppointment(Appointment appointment) {
        appointmentRepository.insertAppointment(appointment);
    }

    public void deleteAppointmentById(int id) {
        appointmentRepository.deleteAppointmentById(id);
    }
}
