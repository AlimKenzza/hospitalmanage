package com.aitu.hospitalmanage.services;


import com.aitu.hospitalmanage.model.Department;
import com.aitu.hospitalmanage.repositories.DepartmentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentService {
    private final DepartmentRepository departmentRepository;

    public DepartmentService(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    public List<Department> getAllDepartments() {
        return departmentRepository.getAllDepartments();
    }


    public Department getDepartmentById(int id) {
        return departmentRepository.getDepartmentById(id);
    }

    public void createDepartment(Department department) {
        departmentRepository.insertDepartment(department);
    }

    public void deleteDepartmentById(int id) {
        departmentRepository.deleteDepartment(id);
    }
}
