package com.aitu.hospitalmanage.services;
import com.aitu.hospitalmanage.model.Doctor;
import com.aitu.hospitalmanage.repositories.CrudRepository;
import com.aitu.hospitalmanage.repositories.DoctorRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DoctorService {
    private final DoctorRepository doctorRepository;

    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public List<Doctor> getAllDoctors() {
        return doctorRepository.findAll();
    }



    public Doctor getDoctorById(int id) {
        return doctorRepository.getDoctorById(id);
    }

    public void createDoctor(Doctor doctor) {
       doctorRepository.insertDoctor(doctor);
    }


    public void deleteDoctorById(int id) {
       doctorRepository.deleteDoctor(id);
    }

    public void updateDoctor(Doctor doctor) {
        doctorRepository.updateDoctor(doctor);
    }
}
