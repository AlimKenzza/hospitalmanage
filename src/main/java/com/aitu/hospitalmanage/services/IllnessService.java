package com.aitu.hospitalmanage.services;


import com.aitu.hospitalmanage.model.Illness;
import com.aitu.hospitalmanage.repositories.IllnessRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IllnessService {
    private final IllnessRepository illnessRepository;

    public IllnessService(IllnessRepository illnessRepository) {
        this.illnessRepository = illnessRepository;
    }

    public List<Illness> getAllInsurances() {
        return illnessRepository.getAllInsurances();
    }

    public void createIllness(Illness illness) {
        illnessRepository.insertIllness(illness);
    }
}
