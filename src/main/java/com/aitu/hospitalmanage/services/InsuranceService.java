package com.aitu.hospitalmanage.services;


import com.aitu.hospitalmanage.model.Insurance;
import com.aitu.hospitalmanage.repositories.InsuranceRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InsuranceService {
    private final InsuranceRepository insuranceRepository;

    public InsuranceService(InsuranceRepository insuranceRepository) {
        this.insuranceRepository = insuranceRepository;
    }

    public List<Insurance> getAllInsurances() {
        return insuranceRepository.getAllInsurances();
    }


    public Insurance getInsuranceById(int id) {
        return insuranceRepository.getInsuranceById(id);
    }

    public Insurance getInsuranceByPatientId(int id) {
        return insuranceRepository.getInsuranceByPatientId(id);
    }

    public void createInsurance(Insurance insurance) {
        insuranceRepository.insertInsurance(insurance);
    }
}
