package com.aitu.hospitalmanage.services;



import com.aitu.hospitalmanage.model.Patient;
import com.aitu.hospitalmanage.repositories.PatientRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientService {

    private final PatientRepository patientRepository;

    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public List<Patient> getAllPatients() {
        return patientRepository.findAll();
    }

    public Patient getPatientById(int id) {
        return patientRepository.getPatientById(id);
    }

    public void createPatient(Patient patient) {
        patientRepository.insertPatient(patient);
    }

    public void deletePatientById(int id) {
        patientRepository.deletePatient(id);
    }
}
