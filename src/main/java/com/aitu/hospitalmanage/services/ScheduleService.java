package com.aitu.hospitalmanage.services;


import com.aitu.hospitalmanage.model.Schedule;
import com.aitu.hospitalmanage.repositories.ScheduleRepository;
import org.springframework.stereotype.Service;

@Service
public class ScheduleService {
    private final ScheduleRepository scheduleRepository;

    public ScheduleService(ScheduleRepository scheduleRepository) {
        this.scheduleRepository = scheduleRepository;
    }


    public Schedule getConnect(int schedule_id, int doctor_id, int appointment_id) {
        return scheduleRepository.getConnect(schedule_id,doctor_id,appointment_id);
    }
}
