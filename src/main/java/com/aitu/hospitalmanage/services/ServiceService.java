package com.aitu.hospitalmanage.services;


import com.aitu.hospitalmanage.model.Service_id;
import com.aitu.hospitalmanage.repositories.ServiceRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceService {
    private final ServiceRepository serviceRepository;

    public ServiceService(ServiceRepository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }

    public List<Service_id> getAllServiceId() {
        return serviceRepository.getAllServiceId();
    }
}
